﻿#include <windows.h>
#include <iostream>
#include <vector>

using namespace std;

class Vector
{
public:
	Vector() : x(0), y(0), z(0), VectorLength(0)
	{}
	void show()
	{
		cout << "\n Введите Х: ";
		cin >> x;

		cout << "\n Введите Y: ";
		cin >> y;

		cout << "\n Введите Z: ";
		cin >> z;

		cout << "\n Вы ввели следующие числа: X = " << x << "; Y = " << y << "; Z = " << z << "\n";

		VectorLength = sqrt(x * x + y * y + z * z);

		cout << "\n Длина вектора: " << VectorLength << ". \n\n\a";
	}
private:
	double x;
	double y;
	double z;
	double VectorLength;
};



int main()
{
	setlocale(LC_ALL, "Russian");

	Vector v;
	v.show();

    return 0;
}